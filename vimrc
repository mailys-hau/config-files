set nocompatible
set enc=utf-8
set visualbell " flash instead of beep when woops
set t_vb= " disable bell, need previous line to works
set ruler " display cursor position
set laststatus=2 " Always display status line

syntax on
set number
set noeol
set t_Co=256
set mouse=a " Enable mouse use
set mousehide
set title
set nostartofline " stop certain movements to go to the first character of the line

set expandtab " use ctrl+v to use a real tab in VIM
set softtabstop=4
set shiftwidth=4
set smarttab
set shiftround
filetype indent plugin on " intelligent auto-indent
set autoindent
set smartindent

set colorcolumn=80
set nolist
set showmatch
set backspace=indent,eol,start " allow backspacing over auto-indent, ...
colorscheme gruvbox " remember to add theme in ~/.vim
set background=dark

set hlsearch " highlight searches
set ignorecase " use insensitive case search...
set smartcase " ...except when using capital letters
set wildmenu " better command line completion
set hidden

" Use Y to copy a line instead of yy
map Y y$

" Map ctrl+L to also turn of search highlight until the next search
nnoremap <C-L> :nohl<CR><C-L>

" Show special characters
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set list
