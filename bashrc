#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# XUTerm black background
#xrdb ~/.Xresources

# Bash completion
if [ -f /etc/bash_completion ]; then
  . ~/bash_completion
fi
complete -cf sudo # Tab completion for sudo

# Colors
if [ -f ~/.dir_colors ]; then
  eval `dircolors ~/.dir_colors`
fi
# LS colors
LS_COLORS=$LS_COLORS:'ow=1;36:di=1;36:fi=0;97:ex=0;92' ; export LS_COLORS
#PATH="$PATH:/home/malou/.conscript/bin"; export $PATH


# Make colorcoding available for everyone

Black='\[\e[0;30m\]'	# Black
Red='\[\e[0;31m\]'		# Red
Green='\[\e[0;32m\]'	# Green
Yellow='\[\e[0;33m\]'	# Yellow
Blue='\[\e[0;34m\]'		# Blue
Purple='\[\e[0;35m\]'	# Purple
Cyan='\[\e[0;36m\]'		# Cyan
White='\[\e[0;37m\]'	# White

# Bold
BBlack='\[\e[1;30m\]'	# Black
BRed='\[\e[1;31m\]'		# Red
BGreen='\[\e[1;32m\]'	# Green
BYellow='\[\e[1;33m\]'	# Yellow
BBlue='\[\e[1;34m\]'	# Blue
BPurple='\[\e[1;35m\]'	# Purple
BCyan='\[\e[1;36m\]'	# Cyan
BWhite='\[\e[1;37m\]'	# White

# Background
On_Black='\[\e[40m\]'	# Black
On_Red='\[\e[41m\]'		# Red
On_Green='\[\e[42m\]'	# Green
On_Yellow='\[\e[43m\]'	# Yellow
On_Blue='\[\e[44m\]'	# Blue
On_Purple='\[\e[45m\]'	# Purple
On_Cyan='\[\e[46m\]'	# Cyan
On_White='\[\e[47m\]'	# White

NC='\[\e[m\]'			# Color Reset

ALERT="${BWhite}${On_Red}" # Bold White on red background

# Aliases definition
if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi

# Set PATH so it includes user's private bin directories
PATH="${HOME}/bin:${HOME}/.local/bin:${PATH}"

# Shell Variable
export VISUAL=vim
export EDITOR="$VISUAL"

# shopt options
# TODO

# Set prompt
if [ -f ~/.bash_pretty_ps1 ]; then
  . ~/.bash_pretty_ps1
fi
PS1='$PROMPT_COMMAND}'
PS1="${Yellow}\u@\h${NC}: ${Blue}\w${NC} \\$ "

# Colored GCC warnings and errors
export GCC_COULORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Function shortcuts
mcdir() { mkdir $1; cd $1; }
jpdev() { pdev; jp; }
transfert() {
  local port=${2:-9000}
  ssh -f -N -L localhost:"$port":localhost:"$port" "$1"
  echo "You can know see $1/localhost:$port at http://localhost:$port"
}
cpx() {
    for file in $(ls -p $1/* | head -n $2); do
        echo $file
        cp $file $3
    done
}
lc() { echo "$1 contains $(($(ls -l $1 | wc -l) - 1)) files."; }
